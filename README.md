# Country and Subdivisions WebApp

> WebApp for Countries data management, consumming a Rest API
> The Country and Subdivisions codes follow the ISO 3166-1 Standard 
> For more information see: https://en.wikipedia.org/wiki/ISO_3166-1

##### JS Framework      : Vue 2.5.x
##### UI Frameworks     : Bootstrap 4.x, Bootsrap Vue 2.x
##### AJAX              : Axios 0.18
##### Client Validation : Vee Validate 2.x
##### Vue Template      : CoreUI 
> Based on Open Source Admin Template CoreUI https://coreui.io/

## Configuration
> By defualt the App connects to the Country Api on http://localhost:3000/api
> For setting the Api URL use an Environment Variable named API_URL 
> and then build the App 

``` bash
# For example in linux
export API_URL='http://my.contryapi/api'
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
