import * as modes from './modes'

export default {
  computed: {
    isViewMode () {
      return this.mode === modes.VIEW_MODE
    },
    isEditMode () {
      return this.mode === modes.EDIT_MODE
    },
    isAddMode () {
      return this.mode === modes.ADD_MODE
    },
    isListMode () {
      return this.mode === modes.LIST_MODE
    }
  }
}
