
export default {
  methods: {
    clearErrors (name) {
      this.api.errors.clear(name)
    },
    hasError (name) {
      return this.errors.has(name) || this.api.errors.has(name)
    },
    getError (name) {
      if (this.errors.has(name)) return this.errors.first(name)
      return this.api.errors.get(name)
    },
    anyError () {
      return this.errors.any() || this.api.errors.any()
    },
    validateBeforeSubmit () {
      this.$validator.validateAll().then(result => { if (result) this.submit() })
    },
    onFail (error) {
      if (error.response) {
        let {data, status, headers} = error.response
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(data)
        console.log(status)
        console.log(headers)
        if (data.errors.description) {
          this.$snotify.error(data.errors.description, 'Server Error')
        }
        this.api.errors.record(data)
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
        this.$snotify.error(`Api Server does not respond.`, 'Server Error')
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      console.log(error.config)
    }
  }
}
