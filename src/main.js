// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate'
import VueSession from 'vue-session'
import Snotify from 'vue-snotify'

import App from './App'
import router from './router'
console.log(process.env.API_URL)
Vue.use(VueAxios, axios.create({ baseURL: process.env.API_URL }))
Vue.use(VueSession)
Vue.use(BootstrapVue)
Vue.use(Snotify, { toast: {
  position: 'centerTop',
  timeout: 10000,
  showProgressBar: false
}})
Vue.use(VeeValidate, { fieldsBagName: 'formFields' })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  }
})
