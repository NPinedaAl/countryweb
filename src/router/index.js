import Vue from 'vue'
import Router from 'vue-router'

import * as modes from '@/components/modes'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'
import CountryList from '@/views/country/CountryList'
import CountryActions from '@/views/country/CountryActions'
import SubdivisionActions from '@/views/country/SubdivisionActions'
import Login from '@/views/Login'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: '/countries',
          redirect: '/countries/list',
          name: 'countries',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'country-list',
              component: CountryList
            },
            {
              path: 'add',
              name: 'add-country',
              component: CountryActions,
              props: {id: 0, mode: modes.ADD_MODE}
            },
            {
              path: 'edit/:id',
              name: 'edit-country',
              component: CountryActions,
              props: (route) => ({id: route.params.id, mode: modes.EDIT_MODE})
            },
            {
              path: 'edit/:id/subdivisions/add',
              name: 'add-subdivision',
              component: SubdivisionActions,
              props: (route) => ({parentId: route.params.id, mode: modes.ADD_MODE})
            },
            {
              path: 'edit/:id/subdivisions/edit/:sid',
              name: 'edit-subdivision',
              component: SubdivisionActions,
              props: (route) => ({parentId: route.params.id, id: route.params.sid, mode: modes.EDIT_MODE})
            },
            {
              path: 'edit/:id/subdivisions/:sid',
              name: 'view-subdivision',
              component: SubdivisionActions,
              props: (route) => ({parentId: route.params.id, id: route.params.sid, mode: modes.VIEW_MODE})
            },
            {
              path: ':id',
              name: 'view-country',
              component: CountryActions,
              props: (route) => ({id: route.params.id, mode: modes.VIEW_MODE})
            }
          ]
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
