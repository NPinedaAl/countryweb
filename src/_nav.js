export default {
  items: [
    {
      title: true,
      name: 'Data',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Countries',
      url: '/countries',
      icon: 'icon-flag'
    },
    {
      name: 'New Country',
      url: '/countries/add',
      icon: 'icon-plus'
    }
  ]
}
